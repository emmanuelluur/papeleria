from django.urls import path
from . import views
urlpatterns = [
    path('nuevo/', views.nuevo_prducto, name="nuevo_producto"),
    path('<int:id>/edita/', views.edita_producto, name="edita_producto"),
    path('', views.producto_index, name="producto_index"),
]