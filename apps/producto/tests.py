from django.test import TestCase
from apps.producto.models import Categoria, Producto
# Create your tests here.

class ProductoTest(TestCase):
    def setUp(self):
        categoria = Categoria.objects.create(nombre="Papeleria")
        Producto.objects.create(
                                codigo="shp01",
                                nombre="Sharpie",
                                descripcion="Color negro",
                                categoria=categoria,
                                precio=25,)
    
    def test_producto_success(self):
        producto = Producto.objects.get(codigo="shp01")
        self.assertEqual(producto.categoria.nombre, "Papeleria")
    
    def test_producto_failed(self):
        producto = Producto.objects.get(codigo="shp01")
        self.assertNotEqual(producto.categoria.nombre, "Eléctronica")