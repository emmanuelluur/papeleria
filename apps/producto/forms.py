from django import forms
from apps.producto.models import Producto


class ProductoForm(forms.ModelForm):
    class Meta:
        model = Producto

        fields = [
            'codigo',
            'nombre',
            'descripcion',
            'precio',
            'categoria',
        ]
