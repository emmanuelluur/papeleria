from django.shortcuts import render, redirect
from apps.producto.models import Producto, Categoria
from apps.producto.forms import ProductoForm
from django.contrib import messages
from django.core.paginator import Paginator
# Create your views here.

def producto_index(request):
    """ Busca productos """
    template_name = "producto_lista.html"
    context = {}
    
    if request.method == "POST":  
        try:
            nombre = request.POST['nombre']
            productos = Producto.objects.filter(nombre__contains=nombre)
            context = {
                'resultados': productos,
            }
            return render(request, template_name, context)
        except ValueError:
            return redirect('producto_index')
        
        
    return render(request, template_name, context)
def nuevo_prducto(request):
    """ Agrega nuevo producto """
    template_name = "form_producto.html"
    if request.method == 'POST':
        form = ProductoForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO, 'Guardado correctamente!')
            return redirect('nuevo_producto')
        else:
            messages.add_message(request, messages.WARNING, 'Ocurrio un error! Nota: Todos los campos son obligatorios!')
            return redirect("nuevo_producto")

    try:
        categoria = Categoria.objects.all()
        context = {
        'titulo': 'Nuevo Producto', 
        'categoria': categoria,
        }
    except Exception as e:
        context = {
            'titulo': 'Nuevo Producto'
        }

    return render(request, template_name, context)

def edita_producto(request, id):
    """ Edita producto existente """
    producto = Producto.objects.get(id=id)
    template_name = "form_producto.html"
    categoria = Categoria.objects.all()
    context = {
        'titulo': 'Edita Producto', 
        'producto': producto,
        'categoria': categoria,
        }
    
    if request.method=='GET':
        form = ProductoForm(instance=producto)
    else:
        form = ProductoForm(request.POST, instance=producto)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO, 'Guardado correctamente!')
            return redirect('edita_producto', id)
        else:
            messages.add_message(request, messages.INFO, 'Error interno!')
            return redirect('edita_producto', id)

    return render(request, template_name, context)